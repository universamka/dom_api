const url = 'https://rickandmortyapi.com/api/character';




function createCharacterCard(character){
    let mainDiv = document.getElementById('m');
    let div = document.createElement('div');
    div.className = "character";

    let left = document.createElement('div');
    left.className = "character-left";
    div.appendChild(left);

    let right = document.createElement('div');
    right.className = "character-right";
    div.appendChild(right);


    let photo = document.createElement('img');
    photo.className = "photo";
    photo.setAttribute("src", character.image);
    left.appendChild(photo);

    let name = document.createElement('p');
    name.className= 'name';
    name.innerText = character.name;
    right.appendChild(name);

    let info = document.createElement('div');
    info.className = 'info';

    let infoItems = [character.species, character.location.name, character.created]
    for (item of infoItems) {
        let infoItem = document.createElement('p');
        infoItem.innerText = item;
        info.appendChild(infoItem);
    }
    left.appendChild(info);

    mainDiv.appendChild(div);
    
}


function start(){

    fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        console.log(rawData);
        createCharacterCard(rawData[1]);
        createCharacterCard(rawData[2]);
        createCharacterCard(rawData[3]);
        createCharacterCard(rawData[4]);
        createCharacterCard(rawData[5]);
        createCharacterCard(rawData[6]);
        createCharacterCard(rawData[7]);
        createCharacterCard(rawData[8]);
        createCharacterCard(rawData[9]);
        createCharacterCard(rawData[10]);
        createCharacterCard(rawData[11]);
        createCharacterCard(rawData[12]);
        createCharacterCard(rawData[13]);
        createCharacterCard(rawData[14]);
        createCharacterCard(rawData[15]);
        createCharacterCard(rawData[16]);
        createCharacterCard(rawData[17]);
        createCharacterCard(rawData[18]);
        createCharacterCard(rawData[19]);

        

        

        
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });

}


start();